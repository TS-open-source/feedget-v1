import axios from 'axios'

import { SERVER_DEPLOY_URL } from '@env'

const api = axios.create({
    baseURL: 'http://localhost:3333'
    //baseURL: process.env.NODE_ENV !== 'development' ? SERVER_DEPLOY_URL : SERVER_DEV_URL
})

export default api